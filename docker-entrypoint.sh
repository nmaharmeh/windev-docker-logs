#!/bin/bash

# turn on bash's job control
set -m

# Start Project + setting for docker mode + add serial key number
./usr/local/WEBDEV/27.0/wd270admind --docker --configuration=N27X012345-X1234567 &

# Tail logs for docker
tail -f /var/log/WindevProject.log > /proc/1/fd/1 &

wait -n
