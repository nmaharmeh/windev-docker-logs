FROM windev/webdev-base:FR270104h

COPY WindevProject.ZIP ${WEBDEVConfiguration}comptes/ftp_webdev/

RUN set -ex \
	&& cd ${WEBDEVBinaries} \
	&& ${WEBDEVBinaries}WDInstalle /DOCKER/INSTALLEGO/WindevProject/REST

# Création de la persistance
VOLUME ${WEBDEVConfiguration}comptes/Donnees/WindevProject/

# Fichier de logs pour Docker
RUN touch /var/log/WindevProject.log

# Nouvel entrypoint
COPY docker-entrypoint.sh /app/docker-entrypoint.sh
RUN chmod +x /app/docker-entrypoint.sh


ENTRYPOINT ["/app/docker-entrypoint.sh"]
